import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { MaterialModule } from "./core/material/material.module";
import { SimpleFormComponent } from "./simple-form/simple-form.component";
import { SharedModule } from "./shared/shared.module";
import { TrainingService } from "./services/training.service";
import { SignupComponent } from "./signup/signup.component";
import { NewTrainingComponent } from "./new-training/new-training.component";

@NgModule({
  declarations: [
    AppComponent,
    SimpleFormComponent,
    SignupComponent,
    NewTrainingComponent
  ],
  imports: [BrowserModule, MaterialModule, SharedModule],
  providers: [TrainingService],
  bootstrap: [AppComponent]
})
export class AppModule {}
