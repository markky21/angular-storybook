import { Exercise } from "./../shared/models/exercise.model";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';

export class TrainingService {
  private exercises: Exercise[] = [
    {
      id: "przysiady",
      name: "Przysiady",
      calories: 12,
      duration: 140
    },
    {
        id: "pompki",
        name: "Pompki",
        calories: 12,
        duration: 140
      }
  ];

  fetchAvailableExercises(): Observable<Exercise[]>{
    return Observable.from([this.exercises]);
  }
}
