import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { Exercise } from './../shared/models/exercise.model';
import { TrainingService } from '../services/training.service';

@Component({
  selector: 'app-new-training',
  templateUrl: './new-training.component.html',
  styleUrls: ['./new-training.component.scss']
})
export class NewTrainingComponent implements OnInit, OnDestroy {
  trainings: Observable<Exercise[]>;
  trainingForm: FormGroup;

  constructor(private trainingService: TrainingService) {}

  ngOnInit() {
    this.trainings = this.trainingService.fetchAvailableExercises();
    this.trainingForm = new FormGroup({
      exercise: new FormControl(null, Validators.required)
    });
  }

  ngOnDestroy(){

  }

  onStartExercise(): void {
    alert('started');
  }
}
