import { storiesOf, moduleMetadata } from "@storybook/angular";

import { SharedModule } from "./../app/shared/shared.module";
import { MaterialModule } from "../app/core/material/material.module";
import { TrainingService } from "./../app/services/training.service";

import { SimpleFormComponent } from "../app/simple-form/simple-form.component";
import { SignupComponent } from "./../app/signup/signup.component";
import { NewTrainingComponent } from "./../app/new-training/new-training.component";

storiesOf("SimpleForm", module)
  .addDecorator(
    moduleMetadata({
      imports: [SharedModule, MaterialModule],
      schemas: [],
      declarations: [],
      providers: []
    })
  )
  .add("Default", () => ({
    component: SimpleFormComponent
  }))
  .add("With props", () => ({
    component: SimpleFormComponent,
    props: {
      title: "Foo"
    }
  }));

storiesOf("SignUp", module)
  .addDecorator(
    moduleMetadata({
      imports: [SharedModule, MaterialModule],
      schemas: [],
      declarations: [],
      providers: []
    })
  )
  .add("Default", () => ({
    component: SignupComponent
  }));

storiesOf("NewTrainingComponent", module)
  .addDecorator(
    moduleMetadata({
      imports: [SharedModule, MaterialModule],
      schemas: [],
      declarations: [],
      providers: [TrainingService]
    })
  )
  .add("Default", () => ({
    component: NewTrainingComponent
  }));
